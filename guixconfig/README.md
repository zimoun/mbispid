# GUIX configuration

## Directory content

This directory contains all files necessary to generate the environment from GUIX
See : https://hpc.guix.info/ and https://guix.gnu.org/

* `channels.scm` : gets the commit number to get reproductible versions of the environment (fixed point of analyses)
* `manifest.scm` : gets the packages stored in GUIX (all packages described upstream required to build the environment)
* `my-pkgs.scm` : gets the packges with reproductible versions that are not stored in GUIX (packages not available upstream are defined here)

## How to reproduce the environment used for these analyses.

Start by installing Guix, read the following ressources:

* [Guix manual](https://guix.gnu.org/manual/en/html_node/Installation.html)
* [Guix-HPC website](https://hpc.guix.info/)
* [Guix reproducibility in fondamental science repository](https://gitlab.com/nivall/guixreprodsci)

Then run `guix pull` to make sure Guix version is up-to-date.

For this project you can run `runguix.sh` file or use the commande line below to load the environment (creating the envrionment may take some time).

```
guix time-machine -C channels.scm -- shell -m manifest.scm -- R
```

Here you have now access to the exact version of the complete stack of software used to perform the analyses of this project.

## Proof of concept through GitlabCI

The results were obtained on one computer that compiled `.Rmd` files to `.HTML` files. The compilation process was performed on a Guix environment in one computer. This computer used Guix to package the environment 
in a container hosted on Docker:  [nivallettours:compute-v1](https://hub.docker.com/layers/nivallettours/mbispid/compute-v1/images/sha256-3450cb300684a23c7334153936ec3048a6f4b8bac67bcb64687fcac203ecddf3?context=explore). The `.yml` file describes the commands used to compile `.Rmd` files. 


The results are hosted here:

* [Main characteristics of patients](https://nivall.gitlab.io/mbispid/mainCharacteristics.html)
* [Characteristics of infectious events](https://nivall.gitlab.io/mbispid/mainInfectionsCharact.html)
* [Cumulative incidence analyses](https://nivall.gitlab.io/mbispid/firstInfection.html)

One may argue that Guix impact here relies on container. But, if the container is lost in the near future, how could we make sure that new environement will be exactly the same and that older packages will still be available and the new stack of software suitable for `<random_package_1>`. Guix can restore the exact whole stack of software required (defined in `manifest.scm` and `my-pkgs/my-pkgs.scm`) and their dependancies with the exact same versions that are defined at a fixed point (`channels.scm`). 

This method enable:

* Transparency by allowing full inspection of environment used
* Reproducibility in the short term by providing a transparent environment that can be shared accross multiple research teams
* Reproducibility in the long term by providing a tool to recreate the environment used in the past which help future project to inspect or reproduce results of a research
