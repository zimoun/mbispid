#!/bin/bash
# Author: Nicolas Vallet
# This script render all Rmd file to HTML

cd notebooks

Rscript -e 'lapply(list.files(), rmarkdown::render, quiet = FALSE)'
